//package com.java.spring.dto;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.java.spring.model.Product;
//import com.java.spring.model.ShoppingCartProduct;
//
//import java.util.List;
//
//public class ShoppingCartProductDto {
//
//    private Product productID;
//    private Integer quantity;
//
//    private List<ShoppingCartProduct> productCarts;
//
//    public ShoppingCartProductDto(Product productID, Integer quantity, List<ShoppingCartProduct> productCarts) {
//        this.productID = productID;
//        this.quantity = quantity;
//        this.productCarts = productCarts;
//    }
//
//    public void setProductID(Product productID) {
//        this.productID = productID;
//    }
//
//    public List<ShoppingCartProduct> getProductCarts() {
//        return productCarts;
//    }
//
//    public void setProductCarts(List<ShoppingCartProduct> productCarts) {
//        this.productCarts = productCarts;
//    }
//
//    public Product getProductID() {
//        return productID;
//    }
//
//    public Integer getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(Integer quantity) {
//        this.quantity = quantity;
//    }
//
//    //    public Product getProductID() {
////        return productID;
////    }
////
////    public void setProduct(Product product) {
////        this.productID = productID;
////    }
////
////    public Integer getQuantity() {
////        return quantity;
////    }
////
////    public void setQuantity(Integer quantity) {
////        this.quantity = quantity;
////    }
//}
