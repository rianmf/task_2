package com.java.spring.controller;

import com.java.spring.model.Product;
import com.java.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    /*
    * ADD PRODUCT
    * */
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", value = "/add")
    public void add(@RequestBody Product product) {
        productService.addProduct(product);
    }
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", value = "/addAll")
    public void add(@RequestBody List<Product> product) {
        productService.addAllProduct(product);
    }


    /*
    * GET PRODUCT
    * */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "/all")
    public @NotNull Iterable<Product> getProducts() {
        return productService.getAllProducts();
    }
    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "get/{id}")
    public Product getProduct(@PathVariable(name = "id") Integer id) {
       return productService.getProduct(id);
    }

    //    @PutMapping(value = "/update/{id}")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json", consumes = "application/json", value = "/update/{id}")
    public void update(@RequestBody Product product, @PathVariable Integer id) {
        productService.updateProduct(id, product);
    }


    //    @DeleteMapping(value = "/delete/{id}")
    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json", value = "/delete/{id}")
    public String delete(@PathVariable(name = "id") Integer id) {
        String result = "";
        if (id == null) {
            result = "id " + id + "tidak ditemukan ";
            return result;
        }
        result = "id " + id + " berhasil di hapus";
        productService.deleteProduct(id);
        return result;
    }


}
