package com.java.spring.controller;

import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import com.java.spring.repository.ShoppingCartProductRepository;
import com.java.spring.service.ShoppingCartProductService;
import com.java.spring.service.ShoppingCartService;
import com.java.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("/api/shoppingCarts")
public class ShoppingCartController {

    @Autowired
    ProductService productService;
    @Autowired
    ShoppingCartService shoppingCartService;
    @Autowired
    ShoppingCartProductService shoppingCartProductService;

    @Autowired
    ShoppingCartProductRepository shoppingCartProductRepository;

    /*
     * BUAT CREATE SHOPPING CART ID
     * */
    @PostMapping("/create")
    public ResponseEntity<ShoppingCart> create() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart = shoppingCartService.create(shoppingCart);
        shoppingCartService.update(shoppingCart);
        return new ResponseEntity<>(shoppingCart, HttpStatus.CREATED);
    }

    /*
     * BUAT MENAMPILKAN ALL SHOPPING CART
     * */
    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public @NotNull Iterable<ShoppingCart> list() {
        return shoppingCartService.getAllCarts();
    }

    /*
     * BUAT MENAMPILKAN SHOPPING CART BY ID
     * */
    @GetMapping("/{shoppingCartId}")
    public @ResponseBody
    ShoppingCart getShoppingCartById(@PathVariable(value = "shoppingCartId") Integer shoppingCartId) {
        return shoppingCartService.getShoppingCart(shoppingCartId);
    }

    /*
     * BUAT MENAMBAHKAN ISI CART BY SHOPPING CART ID
     * */
    @PostMapping("/add/{shoppingCartId}")
    public ShoppingCartProduct save(@PathVariable(value = "shoppingCartId") Integer shoppingCartId,
                                    @RequestParam(name = "productId") Integer productId,
                                    @RequestParam(name = "quantity") Integer quantity){
        return shoppingCartProductService.save(shoppingCartId,productId,quantity);

    }


//    @RequestMapping(value = "/remove/{productId}", method = RequestMethod.PUT)
//    @ResponseStatus(value = HttpStatus.NO_CONTENT)
//    public void removeItem(@PathVariable(value = "productId") Long productId) {
//        ShoppingCartProduct cartItem = shoppingCartProductService.getShoppingCartProductByProductId(productId);
//        shoppingCartProductService.removeShoppingCartProduct(cartItem);
//
//    }



    /*
     * UPDATE CART BY SHOPPING CART ID
     *  */
    @PutMapping("/update")
    public void update(@RequestBody ShoppingCartProduct shoppingCartProduct) {
        shoppingCartProductService.updateCart(shoppingCartProduct);
    }













    /*
     * DELETE SHOPPING CART BY ID
     * */
    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json", value = "/deleteShoppingCart/{id}")
    public String delete(@PathVariable(name = "id") Integer id) {
        String result = "";
        if (id == null) {
            result = "id " + id + "tidak ditemukan ";
            return result;
        }
        result = "id " + id + " berhasil di hapus";
        shoppingCartService.delete(id);
        return result;
    }

    /*
     * DELETE SEMUA ISI CART BY SHOPPING CART ID
     * */
    @RequestMapping(value = "/removeCart/{shoppingCartId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void clearCart(@PathVariable(value = "shoppingCartId") Integer shoppingCartId) {
        ShoppingCart shoppingCart = shoppingCartService.getShoppingCart(shoppingCartId);
        shoppingCartProductService.removeAllShoppingCartProducts(shoppingCart);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illegal request, please verify your payload")
    public void handleClientErrors(Exception ex) {

    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Server Error")
    public void handleServerErrors(Exception ex) {

    }

}