package com.java.spring.repository;

import com.java.spring.model.ShoppingCartProduct;

import com.java.spring.model.ShoppingCartProductKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ShoppingCartProductRepository extends JpaRepository<ShoppingCartProduct, ShoppingCartProductKey> {
}
