package com.java.spring;

import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import com.java.spring.model.ShoppingCartProductKey;
import com.java.spring.service.ProductService;
import com.java.spring.service.ShoppingCartProductService;
import com.java.spring.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ShoppingCartApplication {
    @Autowired
    ShoppingCartService shoppingCartService;

    public static void main(String[] args) {
        SpringApplication.run(ShoppingCartApplication.class, args);
    }

//    @Bean
//    CommandLineRunner runner(ShoppingCartProductService shoppingCartProductService) {
//        return args -> {
//            shoppingCartProductService.save(new ShoppingCartProduct(9,5,6));
//            productService.addProduct(new Product(1L, "TV Set", 300000));

//        };
//    }
//
//    @Bean
//    CommandLineRunner runner(ProductService productService) {
//        return args -> {
//            productService.addProduct(new Product(1, "TV Set", 300000));
//            productService.addProduct(new Product(2, "Game Console", 20000));
//            productService.addProduct(new Product(3, "Sofa", 100000));
//            productService.addProduct(new Product(4, "Icecream", 5000));
//            productService.addProduct(new Product(5, "Beer", 30000));
//            productService.addProduct(new Product(6, "Phone", 5000000));
//            productService.addProduct(new Product(7, "Watch", 30000));
//        };
//    }


}
