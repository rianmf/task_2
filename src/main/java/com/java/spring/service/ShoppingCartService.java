package com.java.spring.service;

import com.java.spring.model.ShoppingCart;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingCartService {

    ShoppingCart create(ShoppingCart shoppingCart);

    Iterable<ShoppingCart> getAllCarts();

    public ShoppingCart getShoppingCart(Integer shoppingCartId);

    void update(ShoppingCart shoppingCart);

    void delete(Integer id);
}
