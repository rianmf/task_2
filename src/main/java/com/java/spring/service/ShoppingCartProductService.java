package com.java.spring.service;

import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShoppingCartProductService {

//    void addShoppingCartProduct(Integer productId, Integer shoppingId, Integer quantity);


    public ShoppingCartProduct save(Integer shoppingCartId, Integer productId, Integer quantity);

    void removeShoppingCartProduct(ShoppingCartProduct shoppingCartProduct);

    void removeAllShoppingCartProducts(ShoppingCart shoppingCart);

    void updateCart(ShoppingCartProduct shoppingCartProduct);

//    void updateCartProduct(ShoppingCartProduct shoppingCartProduct);




//    ShoppingCartProduct getShoppingCartProductByProductId(Long productId);



}

