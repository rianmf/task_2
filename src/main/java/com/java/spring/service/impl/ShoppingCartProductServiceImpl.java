package com.java.spring.service.impl;

import com.java.spring.exception.ResourceNotFoundException;
import com.java.spring.model.Product;
import com.java.spring.model.ShoppingCart;
import com.java.spring.model.ShoppingCartProduct;
import com.java.spring.model.ShoppingCartProductKey;
import com.java.spring.repository.ProductRepository;
import com.java.spring.repository.ShoppingCartProductRepository;
import com.java.spring.repository.ShoppingCartRepository;
import com.java.spring.service.ProductService;
import com.java.spring.service.ShoppingCartProductService;
import com.java.spring.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.*;

@Service
@Transactional
public class ShoppingCartProductServiceImpl implements ShoppingCartProductService {


    @Autowired
    private ShoppingCartProductRepository shoppingCartProductRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingCartService shoppingCartService;


    /*
    * ADD PRODUCT TO CART
    * */
    @Override
    public ShoppingCartProduct save(Integer shoppingCartId, Integer productId, Integer quantity) {

        ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct();
        Product product = productRepository.findById(productId).get();
        ShoppingCart shoppingCart = shoppingCartRepository.findById(shoppingCartId).get();

        shoppingCartProduct.setKey(new ShoppingCartProductKey(shoppingCartId,productId));

        shoppingCartProduct.setQuantity(quantity);
        shoppingCartProduct.setProduct(product);
        shoppingCartProduct.setShoppingCart(shoppingCart);
        shoppingCartProduct.setTotalPrice(product.getPrice() * quantity);

        return shoppingCartProductRepository.save(shoppingCartProduct);
    }

    /*
     * REMOVE ALL ITEM CART
     * */
    @Override
    public void removeShoppingCartProduct(ShoppingCartProduct shoppingCartProduct) {
        shoppingCartProductRepository.delete(shoppingCartProduct);
    }

    @Override
    public void removeAllShoppingCartProducts(ShoppingCart shoppingCart) {
        List<ShoppingCartProduct> shoppingCartProducts = shoppingCart.getCarts();

        for (ShoppingCartProduct item : shoppingCartProducts) {
            removeShoppingCartProduct(item);
        }

    }

    /*
     *
     * */

    /*
     * BUAT UPDATE DATA CART
     * */

    @Override
    public void updateCart(ShoppingCartProduct shoppingCartProduct) {

        shoppingCartProduct.getProduct();
        shoppingCartProduct.getShoppingCart();
        shoppingCartProduct.getQuantity();
        
        shoppingCartProductRepository.saveAndFlush(shoppingCartProduct);

    }
//    @Override
//    public void updateCart(Long id, Product product) {
//        productRepository.saveAndFlush(product);
//    }
//
//    @Override
//    public void updateCartProduct(ShoppingCartProduct shoppingCartProduct) {
//        shoppingCartProductRepository.saveAndFlush(shoppingCartProduct);
//        /*
//         * NGAMBIL ID SHOPPING CART
//         * */
////        ShoppingCart shoppingCart = new ShoppingCart();
////        List<ShoppingCartProduct> shoppingCartProducts = shoppingCart.getCarts();
//        /*
//         * NGAMBIL ID PPRODUCT
//         * */
//
//
//    }
}


//        @Override
//        public void updateShoppingCartProducts (ShoppingCart shoppingCart){
//
//
//        }

//    @Override
//    public ShoppingCartProduct getShoppingCartProductByProductId(Long productId) {
//        return shoppingCartProductRepository
//                .findById(productId)
//                .orElseThrow(() -> new ResourceNotFoundException("Product not found"));
//    }


//
//
//
//    //
//    @Override
//    public ShoppingCartProduct addProductToCart(List<Long> productId, ShoppingCartProduct shoppingCartProduct, Integer quantity) {
//        ShoppingCart shoppingCart = new ShoppingCart();
//
//        List<ShoppingCartProduct> newShoppingCartProduct = new ArrayList<>();
//
//        Integer price = 0;
//        for (Long id : productId) {
//            Product product = this.productService.getProduct(id);
//
//            // product di line atas, itu dimasukkin ke array
//            if (product != null) {
//                shoppingCartProduct.setProduct(product);
//                shoppingCartProduct.getShoppingCart().getId();
//                shoppingCartProduct.setQuantity(quantity);
//
//                price = price + shoppingCartProduct.getQuantity() * product.getPrice();
//                shoppingCartProduct.setTotalPrice(price);
////                quantity++;
////                price = price + product.getPrice();
//            }
//            // set id
//
////            shoppingCartProduct.setProduct(product);
////            shoppingCartProduct.getShoppingCart(shoppingCart.getId());
////            shoppingCartProduct.getQuantity();
////            shoppingCartProduct.setTotalPrice(shoppingCartProduct.getQuantity() * product.getPrice());
//
//            newShoppingCartProduct.add(create(new ShoppingCartProduct(product,
//                                                                      shoppingCart,
//                                                                      quantity,
//                                                                      price)));
//
//        }
//
//
////        // set quantity
////        shoppingCartProduct.setQuantity(quantity);
////        // set total price
////        shoppingCartProduct.setTotalPrice(price);
//
////        shoppingCart.setCarts(newShoppingCartProduct);
//        // save??????
//        shoppingCart.setCarts(newShoppingCartProduct);
//        return shoppingCartProductRepository.save(shoppingCartProduct);
//    }


//}
