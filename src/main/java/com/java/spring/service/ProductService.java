package com.java.spring.service;

import com.java.spring.model.Product;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface ProductService {


    /* get by id*/
    Product getProduct(Integer id);

    /* add product*/
    Product addProduct(Product product);
    public List<Product> addAllProduct(List<Product> product);


    /*get all*/
    List<Product> getAllProducts();

    /* update product*/
    Product updateProduct(Integer id, Product product);

    /* delete product by id*/
    void deleteProduct(Integer id);
}
