package com.java.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "shoppingCartProduct")
public class ShoppingCartProduct {

    @EmbeddedId
    @JsonIgnore
    private ShoppingCartProductKey key;

    @ManyToOne(cascade=CascadeType.ALL)
    @MapsId("shoppingCartID")
    @JoinColumn(name = "shoppingCartID", nullable = false)
    @JsonIgnore
    private ShoppingCart shoppingCart;

    @ManyToOne(cascade=CascadeType.ALL)
    @MapsId("productID")
    @JoinColumn(name = "productID", nullable = false)
    private Product product;

    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "totalPrice")
    private Integer totalPrice;

    public ShoppingCartProduct() {

    }

    public ShoppingCartProduct(ShoppingCart shoppingCart, Product product, Integer quantity) {
        this.shoppingCart = shoppingCart;
        this.product = product;
        this.quantity = quantity;
        this.key = new ShoppingCartProductKey(shoppingCart.getShoppingCartID(),product.getProductID());
    }


    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotalPrice() {
        totalPrice = product.getPrice() * getQuantity();
        return totalPrice;
    }

    public ShoppingCartProductKey getKey() {
        return key;
    }

    public void setKey(ShoppingCartProductKey key) {
        this.key = key;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime * result + ((shoppingCart == null) ? 0 : shoppingCart.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        ShoppingCartProduct other = (ShoppingCartProduct) obj;
        if (shoppingCart == null) {
            if (other.shoppingCart != null)
                return false;
        } else if (!shoppingCart.equals(other.shoppingCart))
            return false;

        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;

        if (quantity == null) {
            if (other.quantity != null)
                return false;
        } else if (!quantity.equals(other.quantity))
            return false;


        if (key == null) {
            if (other.key != null)
                return false;
        } else if (!key.equals(other.key))
            return false;
        return true;
    }

}
