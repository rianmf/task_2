package com.java.spring.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class ShoppingCartProductKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "productID")
    private Integer productID;

    @Column(name = "shoppingCartID")
    private Integer shoppingCartID;

    public ShoppingCartProductKey(){}

    public ShoppingCartProductKey(Integer productID, Integer shoppingCartID) {

        this.productID = productID;
        this.shoppingCartID = shoppingCartID;
    }


    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public Integer getShoppingCartID() {
        return shoppingCartID;
    }

    public void setShoppingCartID(Integer shoppingCartID) {
        this.shoppingCartID = shoppingCartID;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((productID == null) ? 0 : productID.hashCode());
        result = prime * result + ((shoppingCartID == null) ? 0 : shoppingCartID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShoppingCartProductKey other = (ShoppingCartProductKey) obj;
        if (productID == null) {
            if (other.productID != null)
                return false;
        } else if (!productID.equals(other.productID))
            return false;
        if (shoppingCartID == null) {
            if (other.shoppingCartID != null)
                return false;
        } else if (!shoppingCartID.equals(other.shoppingCartID))
            return false;
        return true;
    }
}


