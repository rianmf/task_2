package com.java.spring.model;

import com.fasterxml.jackson.annotation.JsonFormat;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

import java.util.List;


@Entity
@Table(name = "shoppingCart")
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer shoppingCartID;

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "dateCreated")
    private LocalDate dateCreated;

    @Column(name = "totalCartPrice")
    private double totalCartPrice;

    @OneToMany(mappedBy = "shoppingCart", cascade=CascadeType.ALL)
    private List<ShoppingCartProduct> carts;


    public ShoppingCart() {
    }

    public ShoppingCart(Integer shoppingCartID, LocalDate dateCreated, List<ShoppingCartProduct> carts) {
        this.shoppingCartID = shoppingCartID;
        this.dateCreated = dateCreated;
        this.carts = carts;
    }


    public Integer getShoppingCartID() {
        return shoppingCartID;
    }

    public void setShoppingCartID(Integer shoppingCartID) {
        this.shoppingCartID = shoppingCartID;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<ShoppingCartProduct> getCarts() {
        return carts;
    }

    public void setCarts(List<ShoppingCartProduct> carts) {
        this.carts = carts;
    }

    public double getTotalCartPrice() {

        return totalCartPrice;
    }

    public void setTotalCartPrice(double totalCartPrice) {

        this.totalCartPrice = totalCartPrice;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((shoppingCartID == null) ? 0 : shoppingCartID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ShoppingCart other = (ShoppingCart) obj;
        if (shoppingCartID == null) {
            if (other.shoppingCartID != null)
                return false;
        } else if (!shoppingCartID.equals(other.shoppingCartID))
            return false;
        return true;
    }

    //    public List<ShoppingCartProduct> getShoppingCartProducts() {
//        return shoppingCartProducts;
//    }
//
//    public void setShoppingCartProducts(List<ShoppingCartProduct> shoppingCartProducts) {
//        this.shoppingCartProducts = shoppingCartProducts;
//    }

//    @Transient
//    public int getItemOfProducts() {
//        return this.shoppingCartProducts.size();
//    }
}
